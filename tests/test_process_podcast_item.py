import datetime
import socket
import urllib.error
from unittest.mock import patch

import bs4
import pyPodcastParser.Podcast
from pytest import fixture, raises

from getpodcast import Options, SkipPodcast, process_podcast_item


@fixture
def item():
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    return item


@fixture
def options():
    def fn(**kwargs) -> Options:
        options = {f: None for f in Options._fields}
        options.update(**kwargs)
        return Options(**options)

    return fn


def somefile_isfile(fn):
    return fn == "somefile.ogg"


def somefile_getsize(fn):
    return 102 if fn == "somefile.ogg" else 0


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.utime")
def test_process_podcast_item_no_enclosure_url(
    utime, validateFile, try_download_item, options
):
    pod = ""
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    headers = {}
    fromdate = None
    todate = None
    args = options()

    process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", side_effect=somefile_isfile)
@patch("os.utime")
def test_process_podcast_item_validate_existing(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    pod = "test"
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".")

    process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", side_effect=somefile_isfile)
@patch("os.utime")
def test_process_podcast_item_onlynew_skip_podcast(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    pod = "test"
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".", onlynew=True)

    with raises(SkipPodcast):
        process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called_once()


@patch(
    "getpodcast.try_download_item", return_value=(True, 0)
)  # cancel validate and exit
@patch("getpodcast.validateFile", return_value=False)
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", side_effect=somefile_isfile)
@patch("os.utime")
def test_process_podcast_item_resume_download(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    pod = "test"
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".")

    process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_not_called()
    try_download_item.assert_called_once()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item", return_value=(False, 0))  # download success)
@patch("getpodcast.validateFile", return_value=True)
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", return_value=False)
@patch("os.utime")
def test_process_podcast_item_download(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    pod = "test"
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".")

    process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_called_once()
    try_download_item.assert_called_once()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", side_effect=somefile_isfile)
@patch("os.utime")
def test_process_podcast_item_validate_errors(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    validateFile.side_effect = [
        urllib.error.URLError("unittest"),
        urllib.error.HTTPError(None, None, None, None, None),
        socket.timeout(),
    ]
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".")

    for errs in range(3):
        process_podcast_item("test", item, headers, fromdate, todate, args)

    isfile.assert_called()
    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called()


@patch("getpodcast.try_download_item", return_value=(False, 0))  # download success)
@patch("getpodcast.validateFile")
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", return_value=False)
@patch("os.utime")
def test_process_podcast_item_download_errors(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    validateFile.side_effect = [
        # urllib.error.URLError("unittest"),
        urllib.error.HTTPError(None, None, None, None, None),
        socket.timeout(),
    ]
    pod = "test"
    headers = {}
    fromdate = None
    todate = None
    args = options(template="somefile.ogg", root_dir=".")
    for errs in range(2):
        process_podcast_item(pod, item, headers, fromdate, todate, args)

    utime.assert_not_called()
    try_download_item.assert_called()
    validateFile.assert_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_skip_fromdate(
    utime, isfile, getsize, validateFile, try_download_item, item, options
):
    # arrange
    pod = "test"
    headers = {}
    fromdate = datetime.datetime(2022, 1, 2)
    todate = None
    args = options(template="somefile.ogg", root_dir=".")

    # act
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # assert
    utime.assert_not_called()
    isfile.assert_not_called()
    getsize.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
@patch("os.remove")
def test_process_podcast_item_delete_old(
    rm, utime, isfile, getsize, validateFile, try_download_item, item, options
):
    # arrange
    pod = "test"
    headers = {}
    fromdate = datetime.datetime(2022, 1, 2)
    todate = datetime.datetime(2022, 3, 2)
    args = options(template="somefile.ogg", root_dir=".", deleteold=True)

    # act
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # assert

    isfile.assert_called_once_with("somefile.ogg")
    rm.assert_called_once_with("somefile.ogg")
    utime.assert_not_called()
    getsize.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
@patch("os.remove", side_effect=OSError)
def test_process_podcast_item_delete_old_error(
    rm, utime, isfile, getsize, validateFile, try_download_item, item, options
):
    # arrange
    pod = "test"
    headers = {}
    fromdate = datetime.datetime(2022, 1, 2)
    todate = datetime.datetime(2022, 3, 2)
    args = options(template="somefile.ogg", root_dir=".", deleteold=True)

    # act
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # assert

    isfile.assert_called_once_with("somefile.ogg")
    rm.assert_called_once_with("somefile.ogg")
    utime.assert_not_called()
    getsize.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile", return_value=False)
@patch("os.utime")
@patch("os.remove")
def test_process_podcast_item_delete_old_no_file(
    rm, utime, isfile, getsize, validateFile, try_download_item, item, options
):
    # arrange
    pod = "test"
    headers = {}
    fromdate = datetime.datetime(2022, 1, 2)
    todate = datetime.datetime(2022, 3, 2)
    args = options(template="somefile.ogg", root_dir=".", deleteold=True)

    # act
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # assert

    isfile.assert_called_once_with("somefile.ogg")
    rm.assert_not_called()
    utime.assert_not_called()
    getsize.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile", return_value=False)
@patch("os.path.getsize", side_effect=somefile_getsize)
@patch("os.path.isfile", side_effect=somefile_isfile)
@patch("os.utime")
@patch("os.remove")
def test_process_podcast_item_dry_run(
    rm, utime, isfile, getsize, validateFile, try_download_item, item, options
):
    pod = "test"
    headers = {}

    # delete file
    fromdate = datetime.datetime(2022, 1, 2)
    todate = datetime.datetime(2022, 3, 2)
    args = options(dryrun=True, template="somefile.ogg", root_dir=".", deleteold=True)
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # download existing file
    fromdate = datetime.datetime(2022, 1, 1)
    todate = datetime.datetime(2022, 3, 2)
    args = options(dryrun=True, template="somefile.ogg", root_dir=".")
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # download new file
    fromdate = datetime.datetime(2022, 1, 1)
    todate = datetime.datetime(2022, 3, 2)
    args = options(dryrun=True, template="anotherfile.ogg", root_dir=".")
    process_podcast_item(pod, item, headers, fromdate, todate, args)

    # assert
    isfile.assert_called()
    getsize.assert_called()
    validateFile.assert_called()

    rm.assert_not_called()
    utime.assert_not_called()
    try_download_item.assert_not_called()
