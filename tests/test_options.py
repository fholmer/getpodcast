import argparse
from unittest.mock import patch

from pytest import raises

from getpodcast import Options, options


@patch("argparse.ArgumentParser.parse_args")
def test_options_values_from_argparse(parse_args):
    no_options = {f: None for f in Options._fields}
    parse_args.return_value = argparse.Namespace(**no_options)
    opt = options()
    assert isinstance(opt, Options)
    assert opt._asdict() == no_options


@patch("argparse.ArgumentParser.parse_args")
def test_options_print_help_when_empty(parse_args, capfd):
    no_options = {f: None for f in Options._fields}
    parse_args.return_value = argparse.Namespace(**no_options)
    opt = options()
    out = capfd.readouterr().out
    assert out.startswith("usage: ")
    assert opt


@patch("argparse.ArgumentParser.parse_args")
def test_options_no_text_when_true(parse_args, capfd):
    run_options = {f: "" for f in Options._fields}
    run_options["run"] = True
    parse_args.return_value = argparse.Namespace(**run_options)
    opt = options()
    out = capfd.readouterr().out
    assert out == ""
    assert opt


@patch("argparse.ArgumentParser.parse_args")
def test_options_values_always_from_argparse(parse_args):
    no_options = {f: None for f in Options._fields}
    parse_args.return_value = argparse.Namespace(**no_options)

    opt = options(
        date_from="2021-01-01",
        user_agent="Mozilla/5.0",
        root_dir="~/podcast",
        template="{rootdir}/{podcast}/{year}/{date} - {title}{ext}",
    )

    assert isinstance(opt, Options)
    assert opt._asdict() == no_options


@patch("argparse.ArgumentParser.parse_args")
def test_options_verify_hooks_error(parse_args, capfd):
    hooks_options = {f: "" for f in Options._fields}
    hooks_options["hooks"] = ","  # parser will fail
    parse_args.return_value = argparse.Namespace(**hooks_options)
    with raises(SystemExit):
        opt = options()
        assert opt

    out = capfd.readouterr().out
    assert out.startswith("Error parsing hooks string")
