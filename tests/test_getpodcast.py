import datetime
from unittest.mock import patch

from getpodcast import Options, getpodcast


@patch("getpodcast.process_podcast")
def test_getpodcast_no_run(process_podcast, capfd):
    options = {f: None for f in Options._fields}
    podcasts = {"P1": "URN://P1", "P2": "URN://P2"}

    opt = Options(**options)
    getpodcast(podcasts, opt)

    process_podcast.assert_not_called()
    out = capfd.readouterr().out
    assert out == ""


@patch("getpodcast.process_podcast")
def test_getpodcast_run_all(process_podcast, capfd):
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = todate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=True, dryrun=False, date_from=fromdatestr, date_to=todatestr)

    podcasts = {"P1": "URN://P1", "P2": "URN://P2"}
    opt = Options(**options)
    getpodcast(podcasts, opt)

    for name, url in podcasts.items():
        process_podcast.assert_any_call(name, url, {}, fromdate, todate, opt)

    out = capfd.readouterr().out
    assert out == ""


@patch("getpodcast.process_podcast")
def test_getpodcast_user_agent(process_podcast, capfd):
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = todate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(
        run=True,
        dryrun=False,
        user_agent="UA",
        date_from=fromdatestr,
        date_to=todatestr,
    )

    podcasts = {"P1": "URN://P1"}
    opt = Options(**options)
    getpodcast(podcasts, opt)

    process_podcast.assert_called_once_with(
        "P1", "URN://P1", {"User-Agent": "UA"}, fromdate, todate, opt
    )

    out = capfd.readouterr().out
    assert out == ""


@patch("getpodcast.process_podcast")
def test_getpodcast_list(process_podcast, capfd):
    options = {f: "" for f in Options._fields}
    options.update(list=True)

    podcasts = {"P1": "URN://P1", "P2": "URN://P2"}
    opt = Options(**options)
    getpodcast(podcasts, opt)

    process_podcast.assert_not_called()

    out = capfd.readouterr().out

    for name in podcasts.keys():
        assert name in out


@patch("getpodcast.process_podcast")
def test_getpodcast_keywords(process_podcast, capfd):
    options = {f: "" for f in Options._fields}
    options.update(keywords=True)

    podcasts = {}
    opt = Options(**options)
    getpodcast(podcasts, opt)

    process_podcast.assert_not_called()
    out = capfd.readouterr().out
    assert out.startswith("{rootdir}")
