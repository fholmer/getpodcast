from getpodcast import message

tab = "        "
sep = " "
endl = "\n"


def test_message_none(capfd):
    message("")
    out = capfd.readouterr().out
    assert out == tab + sep + endl


def test_message_simple(capfd):
    message("one line")
    out = capfd.readouterr().out
    assert out == tab + sep + "one line" + endl


def test_message_multiline(capfd):
    message("one line")
    message("two lines")
    out = capfd.readouterr().out
    assert out == tab + sep + "one line" + endl + tab + sep + "two lines" + endl


def test_message_buffer_none(capfd):
    message("", wait=True)
    out = capfd.readouterr().out
    assert out == ""


def test_message_buffer_multiline(capfd):
    message("title", wait=True)
    message("sub title")
    out = capfd.readouterr().out
    assert out == "title" + endl + tab + sep + "sub title" + endl
