import datetime
from unittest.mock import Mock, patch

import bs4
import pyPodcastParser.Podcast

from getpodcast import Options, SkipPodcast, process_podcast


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("getpodcast.process_podcast_item")
def test_process_podcast_no_items(process_podcast_item, urlopen, Request):
    content = Mock()
    content.read.return_value = ""
    urlopen.return_value = content

    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todatestr)
    opt = Options(**options)

    process_podcast("P1", "URN://P1", {}, fromdate, todate, opt)
    process_podcast_item.assert_not_called()


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("getpodcast.process_podcast_item")
def test_process_podcast_skip_podcast(process_podcast_item, urlopen, Request):
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todatestr)
    options.update(podcast="P2")
    opt = Options(**options)

    process_podcast("P1", "URN://P1", {}, fromdate, todate, opt)
    process_podcast_item.assert_not_called()
    urlopen.assert_not_called()
    Request.assert_not_called()


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("getpodcast.Podcast")
@patch("getpodcast.process_podcast_item")
def test_process_podcast_one_item(process_podcast_item, Podcast, urlopen, Request):
    podcast = pyPodcastParser.Podcast.Podcast("")
    Podcast.return_value = podcast

    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    podcast.items.append(item)
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todatestr)
    opt = Options(**options)

    headers = {}
    process_podcast("P1", "URN://P1", headers, fromdate, todate, opt)

    Podcast.assert_called_once()
    process_podcast_item.assert_called_once_with(
        "P1", item, headers, fromdate, todate, opt
    )


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("getpodcast.Podcast")
@patch("getpodcast.process_podcast_item")
def test_process_podcast_multiple_items(
    process_podcast_item, Podcast, urlopen, Request
):
    podcast = pyPodcastParser.Podcast.Podcast("")
    Podcast.return_value = podcast

    items = [
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
    ]
    podcast.items.extend(items)
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todatestr)
    opt = Options(**options)

    headers = {}
    process_podcast("P1", "URN://P1", headers, fromdate, todate, opt)

    Podcast.assert_called_once()
    for item in items:
        process_podcast_item.assert_any_call("P1", item, headers, fromdate, todate, opt)


@patch("getpodcast.process_podcast_item")
@patch("os.makedirs")
def test_process_podcast_url_error(makedirs, process_podcast_item):
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todatestr)
    opt = Options(**options)

    process_podcast("P1", "URN://P1", {}, fromdate, todate, opt)

    process_podcast_item.assert_not_called()


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("getpodcast.Podcast")
@patch("getpodcast.process_podcast_item")
def test_process_podcast_skip_item(process_podcast_item, Podcast, urlopen, Request):
    podcast = pyPodcastParser.Podcast.Podcast("")
    Podcast.return_value = podcast
    process_podcast_item.side_effect = SkipPodcast

    items = [
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
        pyPodcastParser.Podcast.Item(bs4.BeautifulSoup()),
    ]
    podcast.items.extend(items)
    fromdate = datetime.datetime(2021, 1, 1)
    fromdatestr = fromdate.strftime("%Y-%m-%d")
    todate = datetime.datetime(2023, 1, 1)
    todatestr = fromdate.strftime("%Y-%m-%d")
    options = {f: "" for f in Options._fields}
    options.update(run=False, dryrun=True, date_from=fromdatestr, date_to=todate)
    opt = Options(**options)

    headers = {}
    process_podcast("P1", "URN://P1", headers, fromdate, todate, opt)

    Podcast.assert_called_once()
    process_podcast_item.assert_called_once_with(
        "P1", items[0], headers, fromdate, todate, opt
    )
