from unittest.mock import mock_open, patch
from urllib.response import addinfo

from getpodcast import validateFile


@patch("os.path.isfile")
def test_validate_file_if_err(isfile):
    def side_effect(fn):
        return fn == "somefile.err"

    isfile.side_effect = side_effect

    headers = {}
    assert validateFile("somefile", 0, 0, "urn://someurl", headers)
    isfile.assert_called_once_with("somefile.err")


def _make_urlopen_response(headers):
    fp = mock_open()()
    fp.closed = False
    return addinfo(fp, headers)


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("os.path.getsize")
@patch("os.path.isfile")
def test_validate_file_if_enclosure_length_match(isfile, getsize, urlopen, Request):
    isfile.side_effect = lambda fn: fn == "somefile"
    getsize.side_effect = lambda fn: 102 if fn == "somefile" else 0
    urlopen.return_value = _make_urlopen_response({})

    headers = {}
    assert validateFile("somefile", 0, 103, "urn://someurl", headers)
    assert validateFile("somefile", 0, 102, "urn://someurl", headers)
    assert validateFile("somefile", 0, 101, "urn://someurl", headers)
    assert not validateFile("somefile", 0, 100, "urn://someurl", headers)
    assert not validateFile("somefile", 0, 104, "urn://someurl", headers)
    assert not validateFile("somefile", 0, 0, "urn://someurl", headers)


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("os.path.getsize")
@patch("os.path.isfile")
def test_validate_file_if_content_length_match(isfile, getsize, urlopen, Request):
    isfile.side_effect = lambda fn: fn == "somefile"
    getsize.side_effect = lambda fn: 102 if fn == "somefile" else 0
    headers = {}

    urlopen.return_value = _make_urlopen_response({"Content-Length": 100})
    assert validateFile("somefile", 0, 0, "urn://someurl", headers)

    urlopen.return_value = _make_urlopen_response({"Content-Length": 101})
    assert validateFile("somefile", 0, 0, "urn://someurl", headers)

    urlopen.return_value = _make_urlopen_response({"Content-Length": 102})
    assert validateFile("somefile", 0, 0, "urn://someurl", headers)

    urlopen.return_value = _make_urlopen_response({"Content-Length": 103})
    assert validateFile("somefile", 0, 0, "urn://someurl", headers)

    urlopen.return_value = _make_urlopen_response({"Content-Length": 104})
    assert not validateFile("somefile", 0, 0, "urn://someurl", headers)


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.path.getmtime")
def test_validate_file_if_time_published_match(
    getmtime, isfile, getsize, urlopen, Request
):
    isfile.side_effect = lambda fn: fn == "somefile"
    getsize.side_effect = lambda fn: 102 if fn == "somefile" else 0
    getmtime.side_effect = lambda fn: 1600000000 if fn == "somefile" else 0
    urlopen.return_value = _make_urlopen_response({})
    headers = {}

    assert validateFile("somefile", 1600000000, 0, "urn://someurl", headers)
    assert not validateFile("somefile", 1600000001, 0, "urn://someurl", headers)


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.path.getmtime")
def test_validate_file_if_time_modified_match(
    getmtime, isfile, getsize, urlopen, Request
):
    isfile.side_effect = lambda fn: fn == "somefile"
    getsize.side_effect = lambda fn: 102 if fn == "somefile" else 0
    getmtime.side_effect = lambda fn: 1600000000 if fn == "somefile" else 0
    headers = {}

    urlopen.return_value = _make_urlopen_response(
        {"Last-Modified": "Sun, 13 Sep 2020 12:26:40 -0000"}
    )
    assert not validateFile("somefile", 0, 0, "urn://someurl", headers)
    assert validateFile("somefile", 1, 0, "urn://someurl", headers)

    urlopen.return_value = _make_urlopen_response(
        {"Last-Modified": "Sun, 13 Sep 2020 13:26:40 -0000"}
    )
    assert not validateFile("somefile", 1, 0, "urn://someurl", headers)
