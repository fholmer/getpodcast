#! /usr/bin/env python3

"""
* Download episodes from 2021-01-01 to 2021-01-31.
* ``date_to`` is optional (YYYY-MM-DD). If not set, the current date will be used.
"""

import getpodcast

opt = getpodcast.options(date_from="2021-01-01", date_to="2021-01-31")

podcasts = {
    "SGU": "https://feed.theskepticsguide.org/feed/sgu",
    "Radiolab": "http://feeds.wnyc.org/radiolab",
}

getpodcast.getpodcast(podcasts, opt)