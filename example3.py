#! /usr/bin/env python3

"""
This example will call the ``compressor`` function every time
a podcast episode is downloaded and validated. ``ffmpeg`` must be installed
for this example to work.
"""

import os
import pathlib
import shutil
import subprocess

import getpodcast


def compressor(*args, **kwargs):
    podcast = kwargs["pod"]
    if podcast != "The Ron Burgundy Podcast":
        return

    if not shutil.which("ffmpeg"):
        getpodcast.message("ffmpeg not available")
        return

    uncompressed = pathlib.Path(kwargs["newfilename"])
    podcast_folder = uncompressed.parent
    compressed = podcast_folder.joinpath(f"__{uncompressed.name}")

    getpodcast.message("Compressing file ...")
    subprocess.run(
        [
            "ffmpeg",
            "-v",
            "quiet",
            "-y",
            "-i",
            str(uncompressed),
            "-filter_complex",
            "compand=attacks=0:points=-80/-900|-45/-15|-27/-9|0/-7|20/-7:gain=5",
            str(compressed),
        ]
    )
    mtime = kwargs["newfilemtime"]
    os.utime(compressed, (mtime, mtime))
    compressed.replace(uncompressed)
    getpodcast.message("Compressing complete")


opt = getpodcast.options(
    date_from="2019-07-30", root_dir="./podcast", hooks="validated=compressor"
)

podcasts = {"The Ron Burgundy Podcast": "https://feeds.megaphone.fm/HSW7933892085"}

getpodcast.getpodcast(podcasts, opt)
