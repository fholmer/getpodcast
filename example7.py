#! /usr/bin/env python3

"""
This example will call the ``tags`` function every time
a podcast episode is downloaded and validated.

``mutagen`` must be installed for this example to work.
Install ``mutagen`` with pip: ``pip install --user mutagen``
"""

import os
import pathlib
import datetime

import mutagen
from mutagen.id3 import Encoding, PCST, TALB, TDES, TIT2

import getpodcast


def tags(*args, **kwargs):
    filepath = pathlib.Path(kwargs["newfilename"])
    rss_item = kwargs["item"]

    # Only mp3 with ID3 support for now
    if not filepath.suffix.lower() == ".mp3":
        getpodcast.message("Skipped tagging")
        return

    file_obj = mutagen.File(filepath)
    if not isinstance(file_obj.tags, mutagen.id3.ID3):
        getpodcast.message("Tagging failed, ID3 format not detected")
        return

    getpodcast.message("Tagging file ...")

    tags: mutagen.id3.ID3 = file_obj.tags
    tags.add(PCST(value=True))
    tags.add(TALB(encoding=Encoding.UTF8, text=kwargs["pod"]))
    tags.add(TDES(encoding=Encoding.UTF8, text=rss_item.description))
    tags.add(TIT2(encoding=Encoding.UTF8, text=rss_item.title))
    file_obj.save()

    mtime = kwargs["newfilemtime"]
    os.utime(filepath, (mtime, mtime))
    getpodcast.message("Tagging complete")


fromdate = datetime.date.today() - datetime.timedelta(days=14)

opt = getpodcast.options(
    date_from=fromdate.isoformat(),
    root_dir="./podcast",
    hooks="validated=tags",
    user_agent="Mozilla/5.0 (compatible)",
)

podcasts = {
    "SGU": "https://feed.theskepticsguide.org/feed/sgu",
    "Radiolab": "http://feeds.wnyc.org/radiolab",
    "The Ron Burgundy Podcast": "https://feeds.megaphone.fm/HSW7933892085",
    "Neebscast": "https://www.blubrry.com/feeds/neebscast.xml",
}

getpodcast.getpodcast(podcasts, opt)
